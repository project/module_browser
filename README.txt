This module lets you browse Drupal modules by dependency. It is useful, for instance, if you install an API module and want to see what other modules are available for that API.

INSTRUCTIONS

1. Install the module as normal.

2. Check out a bunch of Drupal modules from the drupal.org contrib repository: cvs -d:pserver:anonymous:anonymous@cvs.drupal.org:/cvs/drupal-contrib checkout -r <version tag> contributions/modules

Where <version tag> is something like DRUPAL-6 to get all Drupal 6 modules, or DRUPAL-6--1 to only get modules that have reached 1.0 status.

3. Go to admin/settings/module_browser and enter in the directory of the module checkout. Save and click the "Update module index" link to comb the checkout and build an index of dependencies.

4. Go to the Module dependency browser page and browse away!